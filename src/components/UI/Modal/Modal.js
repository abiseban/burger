import React from 'react'
import styl from './Modal.module.css'
import Aux from '../../../hoc/Auxi'
import Backdrop from '../BackDrop/Backdrop'

const modal = (props)=>(
    <Aux>
        <div 
            className={styl.Modal}
            style={{
                transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                opacity : props.show ? '1' : '0'
            }}>
            {props.children}
        </div>
        <Backdrop show={props.show} cancel={props.cancel}/>
    </Aux>
    
);

export default modal;