import React from 'react'
import styl from './Button.module.css'

const btn = (props)=>(
    <button 
        className={[styl.Button , styl[props.btnType]].join(' ')}
        onClick={props.clicked}>
        {props.children}
    </button>
);

export default btn;