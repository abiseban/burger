import React from 'react'
import styl from './Backdrop.module.css'

const backdrop = (props)=>(
    props.show ? <div className={styl.Backdrop} onClick={props.cancel}></div> : null
);

export default backdrop;