import React from 'react'
import NavItems from '../NavigationItems/NavItems'
import DrawerToggle from '../SideDrawer/DrawerToggle/DrawerToggle'

import classes from './Toolbar.module.css'
const ToolBar = (props)=>{
    return(
        <header className={classes.toolbar}>
            <DrawerToggle click={props.openDrawer}/>
            <div>ABIN</div>
            <nav className={classes.Deskonly}><NavItems/></nav>
        </header>
    );
}

export default ToolBar;