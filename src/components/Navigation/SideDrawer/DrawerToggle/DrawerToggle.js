import React from 'react'
import classes from './DrawerToggle.module.css'

const drawertoggle = (props)=>(
    <div onClick={props.click} className={classes.DrawerToggle}>
        <div></div>
        <div></div>
        <div></div>
    </div>
);

export default drawertoggle;