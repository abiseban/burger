import React from 'react'
import NavItems from '../NavigationItems/NavItems'
import BackDrop from '../../UI/BackDrop/Backdrop'
 
import classes from './SideDrawer.module.css'

const SideDrawer = (props)=>{
    let DrawerClass = [classes.SideDrawer,classes.close];
    if(props.open){
        DrawerClass = [classes.SideDrawer,classes.open];
    }
    return(
    <React.Fragment>
        <BackDrop show={props.open} cancel={props.click}/>
        <div className={DrawerClass.join(' ')}>
            <div>ABIN</div>
            <NavItems/>
        </div>
    </React.Fragment>
);}
export default SideDrawer;