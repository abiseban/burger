import React from 'react'
import classes from './Order.module.css'

const Order = (props)=>{
    const ingredients =[];
    for (let ingName in props.ingredients){
        ingredients.push({
            ingredient: ingName,
            amount:props.ingredients[ingName]
        });
    }
    const ingredientsOutput = ingredients.map(ing=>{
        return(<span key={ing.ingredient} className={classes.OrderItem}>{ing.ingredient} ({ing.amount})</span>)
    });
    return(
        <div className={classes.Order}>
            <p>Ingredients: {ingredientsOutput}</p>
            <p>Price: <strong>{props.price}</strong></p>
        </div>
    );
}

export default Order;