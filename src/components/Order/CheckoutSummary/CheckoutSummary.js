import React from 'react'
import Burger from '../../Burger/Burger'
import Button from '../../UI/Button/Button'

const CheckoutSummary = (props)=>{
    return (
        <div style={{
            textAlign: 'center'
        }}>
            <h1>We hope it tastes well!</h1>
            <Burger ingredient={props.ingredient}/>
            <Button 
                btnType='Danger'
                clicked={props.CheckoutCancel}>
                    CANCEL
            </Button>
            <Button 
                btnType='Success'
                clicked={props.CheckoutContinue}>
                    CONTINUE
            </Button>
        </div>
    );
}

export default CheckoutSummary;