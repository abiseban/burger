import React from 'react'
import styl from './BuildControl.module.css'

const buildcontrol = (props) => (
    <div className={styl.BuildControl}>
        <div className={styl.Label}>{props.lable}</div>
        <button onClick={props.less} className={styl.Less} disabled={!props.disableinfo}>Less</button>
        <button onClick={props.more} className={styl.More}>More</button>
    </div>
);

export default buildcontrol;