import React from 'react'
import styl from './BuildControls.module.css'
import BuildControl from './BuildControl/BuildControl'

const controls = [
    {lable:'Salad', type:'salad'},
    {lable:'Bacon', type:'bacon'},
    {lable:'Cheese', type:'cheese'},
    {lable:'Meat', type:'meat'}
];

const buildcontrols = (props)=>(
    <div className={styl.buildcontrols}>
        <p>Current Price : LKR {props.crtprice}</p>
        {controls.map(ctrl=>(
            <BuildControl 
                key={ctrl.lable} 
                lable={ctrl.lable}
                more={()=>props.more(ctrl.type)}
                less={()=>props.less(ctrl.type)}
                disableinfo={props.disableinfo[ctrl.type]}/>
        ))}
        <button onClick={props.purchase} className={styl.OrderButton} disabled={!props.purchaseable}>ORDER NOW</button>
    </div>
);

export default buildcontrols;