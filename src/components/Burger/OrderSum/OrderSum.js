import React from 'react'
import Aux from '../../../hoc/Auxi'
import Button from '../../UI/Button/Button'

const ordersum = (props)=>{
    
    const OrderState= {...props.ordersum};
    const OrderSum = Object.keys(OrderState).map( igKey => {
        return(
            <li key={igKey}>
                <span style={{textTransform:'capitalize'}}>{igKey}</span> : {OrderState[igKey]}</li>
        );
    });
    
    return(
        <Aux>
            <p>Your Order Summary</p>
            <ul>
                {OrderSum}
            </ul>
            <p>Total Price: {props.price} </p>
            <p>Continue to Chekout?</p>
            <Button btnType='Danger' clicked={props.cancel}>CANCEL</Button>
            <Button btnType='Success' clicked={props.cnt}>CONTINUE</Button>
        </Aux>
    );
}

export default ordersum;