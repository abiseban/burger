import React from 'react'
import BurgerIngredient from './BurgerIngredient/BurgerIngredient'
import styl from './Burger.module.css'

const burger = (props) => {
    let tfrIngredient = Object.keys(props.ingredient).map(igKey=>{
        return [...Array(props.ingredient[igKey])].map((_,i)=>{
            return <BurgerIngredient key={igKey + i} type={igKey}/>
        });
    }).reduce((arr,el)=>{
        return arr.concat(el);
    },[]);
    
    if(tfrIngredient.length===0){
        tfrIngredient= <p>Please add some ingredient!</p>;
    }

    return(
        <div className={styl.burger}>
            <BurgerIngredient type="bread-top"/>
            {tfrIngredient}
            <BurgerIngredient type="bread-bottom"/>
        </div>
    );
}

export default burger;