import React from 'react'
import Aux from '../../hoc/Auxi'
import styl from './Layout.module.css'
import ToolBar from '../Navigation/Toolbar/Toolbar'
import SideDrawer from '../Navigation/SideDrawer/SideDrawer'



const Layout = (props)=>{
const [state,changeState] = React.useState(false);

const CloseHandler = ()=>{
    changeState(false);
}

const OpenHandler = ()=>{
    changeState(true);
}
    return(
        <Aux>
            <SideDrawer open={state} click={CloseHandler}/>
            <ToolBar openDrawer={OpenHandler}/>
            <main className={styl.content}>
                {props.children}
            </main>
        </Aux>
    );
}

export default Layout;