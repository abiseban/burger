import React , {Component} from 'react'
import Order from '../../components/Order/Order'
import axios from '../../axios-orders'
import Spinner from '../../components/UI/Spinner/Spinner'
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler'

class Orders extends Component {
    state={
        orders:[],
        loading: true,
        error:false
    }

    componentDidMount(){
        axios.get('/orders')
            .then(res=>{
                const fetchedOrders = [];
                for (let key in res.data){
                    fetchedOrders.push({
                        ...res.data[key],
                        id:key
                    });
                }
                this.setState({orders:fetchedOrders,loading:false});
            })
            .catch(err=>{
                this.setState({loading:false,error:true});
            });
    }

    render(){
        let orders = (this.state.orders.map(order=>{
            return <Order key={order.id} price={order.price} ingredients={order.ingredients}/>
        }));
        if(this.state.loading){
            orders=<Spinner/>;
        }
        else if(this.state.error){
            orders=<center><p>Please Check your connection and reload the page!</p></center>;
        }
        return(
            <React.Fragment>
                {orders}
            </React.Fragment>
        );
    }
}


export default withErrorHandler(Orders,axios); 