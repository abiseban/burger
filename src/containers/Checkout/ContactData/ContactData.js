import React , {Component} from 'react'
import Button from '../../../components/UI/Button/Button'
import classes from './ContactData.module.css'
import axios from '../../../axios-orders'
import Spinner from '../../../components/UI/Spinner/Spinner'
import Input from '../../../components/UI/Input/Input'


class ContactData extends Component{
    state={
        orderForm: {
            name : {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Name'
                },
                value: ''
            },
            email : {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Your E-mail'
                },
                value: ''
            } ,
            street: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Street'
                },
                value: ''
            },
            city: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'City'
                },
                value: ''
            },
            deliveryMethod: {
                elementType: 'select',
                elementConfig: {
                    options : [
                        {value:'fastest',displayValue:'Fastest'},
                        {value:'cheapest',displayValue:'Cheapest'}
                    ]
                },
                value: ''
            }

        },
        loading: false
    }

    orderHandler = (event)=>{
        event.preventDefault();
        const formData = {}
        for ( let key in this.state.orderForm){
            formData[key]= this.state.orderForm[key].value
        }
        this.setState({loading: true})
        const order={
            ingredients : this.props.ingredients,
            price : this.props.price,
            orderData: formData
            
        }
        axios.post('/orders.json',order)
            .then( res => {
                this.setState({loading: false})
                this.props.history.push('/');
            })
            .catch( err => {
                this.setState({loading: false})
            });
    }

    inputChangedHandler= (event,inputIdentifier)=>{
        const updatedOrder = {...this.state.orderForm};
        const updatedFromElement = {
            ...updatedOrder[inputIdentifier]
        };
        updatedFromElement.value=event.target.value;
        updatedOrder[inputIdentifier]=updatedFromElement;
        this.setState({orderForm:updatedOrder});

    }

    render(){
        const fromElementArray= []
        for( let key in this.state.orderForm){
            fromElementArray.push({
                id:key,
                config:this.state.orderForm[key]
            });
        }

        let form = (
            <form onSubmit={this.orderHandler}>
                    {fromElementArray.map(formElement=>(
                        <Input key={formElement.id} 
                            elementType={formElement.config.elementType}
                            elementConfig={formElement.config.elementConfig}
                            value={formElement.config.value}
                            changed={(event)=>this.inputChangedHandler(event,formElement.id)}/>
                    ))}
                    <Button btnType='Success'>ORDER</Button>
                </form>
        );
        if(this.state.loading){
            form = <Spinner/>
        }
        return(
            <div className={classes.ContactData}>
                <h4>Enter your Contact Data</h4>
                {form}
            </div>
        );
    }
}

export default ContactData;