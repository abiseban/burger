import React , {Component} from 'react'
import Aux from '../../hoc/Auxi'
import Burger from '../../components/Burger/Burger'
import BuildControls from '../../components/Burger/BuildControls/BuildControls'
import Modal from '../../components/UI/Modal/Modal'
import OrderSum from '../../components/Burger/OrderSum/OrderSum'
import axios from '../../axios-orders'
import Spinner from '../../components/UI/Spinner/Spinner'
import withErrorHnd from '../../hoc/withErrorHandler/withErrorHandler'


const ING_PRICES = {
    salad : 50,
    bacon : 50,
    cheese : 100,
    meat : 150
}

class BurgerBuilder extends Component{
    state = {
        ingredient : null,
        totalPrice:150,
        purchaseable:false,
        purchasing: false,
        loading: false,
        error : false
    }
    
    componentDidMount(){
        axios.get('https://react-burger-builder-abin.firebaseio.com/ingredients.json')
            .then(res=>{
                this.setState({ingredient:res.data});
            })
            .catch(err=>{
                this.setState({error:true})
            });
    }

    updatePurchHandler = (ing)=>{
        const sum = Object.keys(ing).map(Igkey=>ing[Igkey]).reduce((sum,el)=>{return sum+el},0);
        this.setState({purchaseable:sum>0})
    }

    addIngHandler = (type)=>{
        const oldcount = this.state.ingredient[type];
        const newcount = oldcount +1;
        const updatedIng = {...this.state.ingredient };
        updatedIng[type]=newcount;
        const priceAddition = ING_PRICES[type];
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice + priceAddition;

        this.setState({ingredient:updatedIng,totalPrice:newPrice});
        this.updatePurchHandler(updatedIng);
    }

    removeIngHandler = (type)=>{
        const oldcount = this.state.ingredient[type];
        if(oldcount<=0){
            return;
        }
        const newcount = oldcount - 1;
        const updatedIng = {...this.state.ingredient };
        updatedIng[type]=newcount;
        const priceAddition = ING_PRICES[type];
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice - priceAddition;

        this.setState({ingredient:updatedIng,totalPrice:newPrice});
        this.updatePurchHandler(updatedIng);
    }

    purchasingHandler = ()=>{
        this.setState({purchasing:true})
    }

    cancelPurchHandler = ()=>{
        this.setState({purchasing:false})
    }

    continuePurchHandler = ()=>{
        const queryParams = [];
        for ( let i in this.state.ingredient){
            queryParams.push(encodeURIComponent(i)+'='+encodeURIComponent(this.state.ingredient[i]))
        }
        queryParams.push('price='+this.state.totalPrice);
        const queryString = queryParams.join('&');
        this.props.history.push({
            pathname:'/checkout',
            search: '?' + queryString
        });
    }

    render(){
        const disableinfo = {...this.state.ingredient};
        for(let key in disableinfo){
            disableinfo[key]= disableinfo[key]>0;
        }
        let ordersum = null;

        let burger = this.state.error ? <center><p>Please Check your connection and reload the page!</p></center> : <Spinner/>;

        if(this.state.ingredient){
            burger = (
            <Aux>
                <Burger ingredient={this.state.ingredient}/>
                <BuildControls
                    crtprice={this.state.totalPrice} 
                    more={this.addIngHandler} 
                    less={this.removeIngHandler}
                    disableinfo={disableinfo}
                    purchaseable={this.state.purchaseable}
                    purchase={this.purchasingHandler}/>
            </Aux>
            );

            ordersum = <OrderSum ordersum={this.state.ingredient} price={this.state.totalPrice} cancel={this.cancelPurchHandler} cnt={this.continuePurchHandler}/>;
        }

        if(this.state.loading){
            ordersum = <Spinner/>
        }

        return(
            <Aux>
                <Modal show={this.state.purchasing} cancel={this.cancelPurchHandler}>
                    {ordersum}
                </Modal>
                {burger}
            </Aux>
        );
    }
}

export default withErrorHnd(BurgerBuilder,axios);